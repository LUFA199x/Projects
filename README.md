# I have utilized Kubernetes as the hosting platform for a system based on microservices architecture. Additionally, I have implemented an Infrastructure as Code (IaaC) tool to automate the provisioning process and optimize the deployment workflow, ensuring that service deployments are both fast and consistent.

### Setup Details according to the Project memo

+ Provision a webapp of your choosing with nginx/httpd frontend proxy and a database (mongo, postgresql etc) backend .
+ Provision the Socks Shop example microservice application - https://microservices-demo.github.io/

### Prerequisites:
+ AWS Account
+ AWS CLI installed
+ AWS IAM Authenticator installed
+ Kubectl installed
+ Terraform
+ Helm
+ ArgoCD
+ Docker
+ Github repo
